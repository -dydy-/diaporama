<!DOCTYPE HTML>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Demo de diaporama</title>
		<!-- Use local files -->
		<link href="css/diapo.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css" />
		<style>
			body{
				width: 900px;
				height: 600px;
				text-align: center;
				margin: 0 auto;
			}
		</style>
	</head>
	
	<body>
		<div class="title">
			<h1>Test du diaporama</h1>
			<?php include("diaporama.php")?>
		</div>
	</body>