<div class="full_diapo">
	<div class="thumbnail">
		<div class="thumbnail_content mCustomScrollbar" data-mcs-theme="minimal">
		<table>
			<tr>
				<td><img class="image_thumb" src="images/photo_1.jpg" /></td>
				<td><img class="image_thumb" src="images/photo_2.jpg"/></td>
				<td><img class="image_thumb" src="images/photo_3.jpg"/></td>
			</tr>
			<tr>
				<td><img class="image_thumb" src="images/photo_4.jpg"/></td>
				<td><img class="image_thumb" src="images/photo_5.jpg"/></td>
				<td><img class="image_thumb" src="images/photo_6.jpg"/></td>
			</tr>
			<tr>
				<td><img class="image_thumb" src="images/photo_7.jpg"/></td>
				<td><img class="image_thumb" src="images/photo_8.jpg"/></td>
				<td><div class="case_vide"></div></td>
			</tr>
		</table>
		</div>
	</div>
	<div class="main_picture">
		<p>
		</p>
	</div>
</div>
<script src="js/jquery-2.1.0.min.js"></script>
<script src="js/jquery.mCustomScrollbar.min.js"></script>
<!-- Use CDN -->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
<script>
	$(document).ready(function(){
		$("#thumbnail").mCustomScrollbar({
			theme:"minimal"
		});
		
		$(".image_thumb").first().clone().appendTo($(".main_picture p"));
		$(".main_picture p").removeAttr("class");
		
		$("td").children().on('click', function(){
			if($(this).hasClass('image_thumb')){
				$('.main_picture').hide();
				$('.main_picture img').attr("src", $(this).attr('src'));
				$('.main_picture').fadeIn("1000");
			}
		});
	});
</script>