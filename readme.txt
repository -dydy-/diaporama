=========================================================================================
=========================================================================================
===========================		Diaporama V.1.0		=========================
=========================================================================================
=========================================================================================

README r�dig� par Ranavy Hong
http://www.ran-dev.com


Diaporama r�alis� en PHP/JavaScript/jQuery
Elle utilise la library jQuery et le plugin de Malihu malihu-custom-scrollbar-plugin disponible ici :
https://github.com/malihu/malihu-custom-scrollbar-plugin



I�) INSTALLATION DU DIAPORAMA
=============================

	D�zipper le fichier "diaporama.zip". V�rifiez que ce fichier contient bien les dossiers et fichiers suivants: 

	- /css (diapo.css, jquery.mCustomScrollbar.min.css)	 --> dossier contenant le style
	- /js (jquery.mCustomScrollbar.min.js)			 --> dossier contenant les fichiers js
	- /images						 --> dossier contenant les images du diaporama
 	- diaporama.php 					 --> le diaporama
	- test-diapo.php					 --> d�mo du diaporama en php
	- readme.txt						 --> ce fichier

	Copiez tout le contenu de l'archive � la racine de votre site web.


	1. Installation du style
	========================

		Ajoutez les 2 lignes de code suivantes entre vos balises <head></head> :
			<link href="css/diapo.css" rel="stylesheet" type="text/css" />
			<link href="css/jquery.mCustomScrollbar.min.css" rel="stylesheet" type="text/css"/>

	2. Insertion du diaporama
	=========================
		
		Ins�rer ce code l� o� vous voulez placer votre diaporama :
			<?php include ("diaporama.php"); ?>
	


II�) MODIFICATION DU DIAPORAMA
==============================

	1. Modification des images
	==========================

		Placez vos images dans le dossier /images.
	Ouvrez le fichier "diaporama.php" et rep�rez la partie suivante :
	
	..
	<table>
		<tr>
			<td><img class="image_thumb" src="images/photo_1.jpg"/></td> // Image 1
			<td><img class="image_thumb" src="images/photo_2.jpg"/></td> // Image 2
			<td><img class="image_thumb" src="images/photo_3.jpg"/></td> // Image 3
		</tr>
		<tr>
			<td><img class="image_thumb" src="images/photo_4.jpg"/></td> // Image 4
			<td><img class="image_thumb" src="images/photo_5.jpg"/></td> // Image 5
			<td><img class="image_thumb" src="images/photo_6.jpg"/></td> // Image 6
		</tr>
		<tr>
			<td><img class="image_thumb" src="images/photo_7.jpg"/></td> // Image 7
			<td><img class="image_thumb" src="images/photo_8.jpg"/></td> // Image 8
			<td><div class="case_vide"></div></td> 			     // Image 9
		</tr>
	</table>
	..
	
	La disposition du diaporama par d�faut est la suivante :
	
		Image 1	Image 2	Image 3
		Image 4	Image 5	Image 6
		Image 7	Image 8	Image 9

	
	2. Ajouter/Enlever des images
	=============================

		Pour ajouter des images, il suffit de copier le contenu d'une balise <tr></tr> et 
	de le placer apr�s le dernier </tr>, puis de modifier ces lignes (voir II.1).
	
	Reprenons notre fichier "diaporama.php" :
	..
			<td><img class="image_thumb" src="images/photo_9.jpg"/></td> // Image 9
		</tr>
		<tr>
			<td><img class="image_thumb" src="images/photo_10.jpg"/></td> // Image 10
			<td><img class="image_thumb" src="images/photo_11.jpg"/></td> // Image 11
			<td><img class="image_thumb" src="images/photo_12.jpg"/></td> // Image 12
		</tr>
	</table>
	..
	
	Le diaporama ressemblera alors � ceci :

		Image 1	Image 2	Image 3
		Image 4	Image 5	Image 6
		Image 7	Image 8	Image 9
		Image 10Image 11Image 12

	Si vous n'avez pas 3*X photos, vous pouvez ajouter des cases vides en mettant la ligne suivante : 
		<td><div class="case_vide"></div></td>
	ex : J'ai 7 photos. Le fichier "diaporama.php" deviendra :
	..
		<tr>
			<td><img class="image_thumb" src="images/ma_photo.jpg"/></td> // Image 7
			<td><div class="case_vide"></div></td> 			      // Image 8
			<td><div class="case_vide"></div></td>			      // Image 9
		</tr>
	</table>
	..

	Pour enlever des images, il suffit de supprimer les balises <tr></tr> et leur contenu.
	ex : J'ai 5 photos. Le fichier "diaporama.php" deviendra :

	..
	<table>
		<tr>
			<td><img class="image_thumb" src="images/ photo_1.jpg"/></td> // Image 1
			<td><img class="image_thumb" src="images/ photo_2.jpg"/></td> // Image 2
			<td><img class="image_thumb" src="images/ photo_3.jpg"/></td> // Image 3
		</tr>
		<tr>
			<td><img class="image_thumb" src="images/ photo_4.jpg"/></td> // Image 4
			<td><img class="image_thumb" src="images/ photo_5.jpg"/></td> // Image 5
			<td><div class="case_vide"></div></td>  		      // Image 6
		</tr>
	</table>
	..

